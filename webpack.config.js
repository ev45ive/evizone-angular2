var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var validator = require('webpack-validator')

module.exports = validator({
    entry: [
        './src/main.ts'
    ],
    output:{
        path: path.join(__dirname,'./dist/'),
        filename: '[name].js'
    },
    resolve:{
        extensions: ['','.ts','.js','.json']
    },
    module:{
        loaders:[
            { test:/\.ts$/, loader:'awesome-typescript-loader' },
            { test:/\.html$/, loader: 'html'},
            { test:/\.css$/, loader: ExtractTextPlugin.extract('style-loader','css-loader')},
            //{ test:/\.css$/, loaders: ['style','css']},
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file?name=assets/[name].[hash].[ext]'
            },
        ]
    },
    plugins:[
        new ExtractTextPlugin('styles.css'),
        
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true
        })
    ]
})