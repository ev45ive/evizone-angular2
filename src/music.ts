
interface Serializable {
    id: string
}

export interface Album extends Serializable {
    genre: Genre
}

export enum Genre {
    pop, rock, classical, soundtrack
}

export interface Track {

}

export interface Playlist {
    tracks: Track[]
}

var a = <Album>{};
a.genre = Genre.classical;



interface MUSIC_OPTIONS {
    /**
     *  Your music server url
     *  @author sages
     */
    server: string;
    tags: { [key: string]: boolean | number }
}

//let query: <T>(item:T) => boolean;
//declare function query<T>(item:T): boolean

interface Collection<T> {
    items: Array<T>
    addItem(item: T): void
    filter(query: (item: T) => boolean): Array<T>
}

class Collectibles {

}

const Music = class extends Collectibles implements Collection<Playlist>{

    items: Playlist[]

    addItem(item: Playlist) {
        this.items.push(item)
    }

    filter(fn: (item: Playlist) => boolean) {
        return this.items.filter(fn);
    }

    constructor(private options?: MUSIC_OPTIONS) {
        super()
        // if(options) this.options = options
        console.log(this.options)
    }

    createPlaylist() {

    }
}

export default Music;